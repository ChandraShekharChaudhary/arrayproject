
function each(elements, cb) {
  if(!Array.isArray(elements) || typeof cb !=='function'){
    console.log(elements);
    return;
  }
    for (let index = 0; index < elements.length; index++) {
      cb(elements[index], index);
    }
    return;
  }
  
  module.exports=each;
  