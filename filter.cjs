function filter(elements, cb) {
    if (!Array.isArray(elements) || typeof cb !== 'function') {
        return [];
    }
    const evenResult = [];
    for (let index = 0; index < elements.length; index++) {
        if (cb(elements[index], index, elements) === true) {
            evenResult.push(elements[index]);
        }
    }
    return evenResult;

}

module.exports = filter;
