
function flatHelper(element, depth){
    const arr=[];
    return flat(element, depth , arr);
}

function flat(elements, depth, arr) {
    
    if (!Array.isArray(elements)) {
        return elements;
    }

    if(depth === undefined){
        depth=1;
    }


    for(let index=0; index<elements.length; index++){
        if(Array.isArray(elements[index]) && depth>=1){
            flat(elements[index], depth-1,arr);
        }else if(elements[index] !== undefined){
            arr.push(elements[index]);
        }
    }
    return arr;
}

module.exports = flatHelper;
