//function reduce(elements, cb, startingValue) {
    // Do NOT use .reduce to complete this function.
    // How reduce works: A reduce function combines all elements into a single value going from left to right.
    // Elements will be passed one by one into `cb` along with the `startingValue`.
    // `startingValue` should be the first argument passed to `cb` and the array element should be the second argument.
    // `startingValue` is the starting value.  If `startingValue` is undefined then make `elements[0]` the initial value.

//}


function reduce(elements, cb, startingValue) {
    if (!Array.isArray(elements) || typeof cb !== 'function' || elements.length === 0) {
        return undefined;
    }

    let acc=startingValue;
    let start=0;
    if(startingValue=== undefined){
        acc=elements[0];
        start=1;
    }
    for(let index=start; index<elements.length;index++){
        acc=cb(acc ,elements[index],index,elements);
    }
    return acc;
}

module.exports = reduce;
