const filterFunction=require('../filter.cjs')
const givenData=[1, 2, 3, 4, 5, 5];


function cb(element) {
    if (element%2==0) {
        return true;
    }
}

const result = filterFunction(givenData,cb);

console.log(result);

function cb2(element){
    if(element*3>=10){
        return true;
    }
}
console.log(filterFunction(givenData,cb2));

console.log(filterFunction([], cb));
console.log(filterFunction({}, cb));
console.log(filterFunction('TestText', cb));