const flat=require('../flatten.cjs')

const element=[1, [2], [[3]], [[[4]]]];


// const result = flat.flatten(element);

console.log(flat());
console.log(flat(1,2));
console.log(flat(element,1));
console.log(flat(element));
console.log(flat(element,3));
console.log(flat(element,50));
console.log(flat({a:[1,2], b:[4,5,6]}));
console.log(flat([1, [2], [[3]], [[[4]]],]));
console.log(flat([1, [2], [[3]], [[[4]]],,,,]));
