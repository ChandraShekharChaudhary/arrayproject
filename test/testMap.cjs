const map=require('../map.cjs')
const givenData=[1, 2, 3, 4, 5, 5];

function cb(indexData,index) {
    return indexData+index;
}

function cb2(indexData,index){
    return indexData*2;
}
const result = map(givenData, cb);
console.log(result);

console.log(map(givenData, cb2));
console.log(map(givenData, 50));
console.log(map(50, cb2));

console.log(map('givenData', cb2));
